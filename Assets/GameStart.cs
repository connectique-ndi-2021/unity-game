using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameData data = FindObjectOfType<GameData>();

        data.score = 0;
        data.level = 1;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
