using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class ConveyorBelt : MonoBehaviour
{

    public UnityEvent dropInWaterEvent;
    
    public Vector3 SpawnPoint;

    public float dropPointOffset;

    public float moveSpeed;
    public float waterOffset;

    public int damage = 1;

    public bool dropped = false;

    private Rigidbody2D rigidbody;

    public Vector3 throwVelocity;
    public float throwVelocityRandom;


    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();

        if(rigidbody is null)
        {
            gameObject.AddComponent<Rigidbody2D>();
        }


        rigidbody.isKinematic = true;
        
        

        this.transform.position = SpawnPoint;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = new Vector3(this.transform.position.x + moveSpeed * Time.deltaTime, this.transform.position.y, this.transform.position.z);

        if(this.transform.position.x > SpawnPoint.x + dropPointOffset)
        {
            if (!dropped)
            {
                
                DropTrash();
            }
            

        }

        if(this.transform.position.y < SpawnPoint.y + waterOffset)
        {
            dropInWaterEvent.Invoke();

            Destroy(gameObject);
        }

    }

    private void DropTrash()
    {
        dropped = true;

        rigidbody.isKinematic = false;
        rigidbody.velocity = throwVelocity * UnityEngine.Random.Range(1-throwVelocityRandom, 1+throwVelocityRandom);

        FindObjectOfType<GameManager>().trashDropped(damage);

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(SpawnPoint, 0.5f);


        Gizmos.DrawWireSphere(SpawnPoint + new Vector3(dropPointOffset, 0, 0), 0.5f);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(SpawnPoint + new Vector3(dropPointOffset, waterOffset, 0), 0.5f);
    }


}
