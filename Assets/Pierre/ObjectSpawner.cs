using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{

    public float spawnDelay;

    public List<GameObject> gameObjects;


    
    public float time;

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if(spawnDelay < time)
        {

            int r = Random.Range(0, gameObjects.Count - 1);

            Instantiate(gameObjects[r], this.transform.position, Quaternion.identity);


            time -= spawnDelay;
        }
        


    }
}
