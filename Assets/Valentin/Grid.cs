using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public enum Occupation { Free, Blocked, Occupied };

    public int height = 20;
    public int width = 10;
    public float obstacleRate = 0.1f;
    public GameObject cell;
    public GameObject[,] grid;
    public Occupation[,] grid_occupation;

    private void Awake()
    {
        grid = new GameObject[height, width];
        grid_occupation = new Occupation[height, width];

    }

    // Start is called before the first frame update
    void Start()
    {
        Generate();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Generate()
    {
        for (int i = 0; i < height; i++)
            for (int j = 0; j < width; j++)
                grid_occupation[i, j] = Occupation.Free;

        float xScale = cell.transform.localScale.x;
        float yScale = cell.transform.localScale.y;

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                
                if (Random.Range(0f, 1f) > obstacleRate)
                {
                    grid[i, j] = Instantiate(cell, new Vector3(transform.position.x + j * xScale, transform.position.y + i * yScale, 1), Quaternion.identity, transform);
                    grid[i, j].GetComponent<Cell>().coordinates = new Vector2Int(i, j);
                }
                else
                {
                    grid_occupation[i, j] = Occupation.Blocked;
                }
            }
        }
    }

    internal bool DoesShapeFits(Vector2 gridCoordinates, Shape pickedUpItem)
    {
        for (int i = 0; i < pickedUpItem.coords.Count; i++)
        {
            Vector2Int shapePosition = pickedUpItem.GetCurrentCoordinate(i);

            int x = Mathf.RoundToInt(gridCoordinates.x) + shapePosition.y;
            int y = Mathf.RoundToInt(gridCoordinates.y) + shapePosition.x;

            if (x < 0 || x >= height) return false;
            if (y < 0 || y >= width) return false;
            if (grid[x, y] == null) return false;
            if (grid_occupation[x, y] != Occupation.Free) return false;

        }
        return true;
    }

    internal void PlaceShape(Vector2 gridCoordinates, Shape pickedUpItem)
    {
        for (int i = 0; i < pickedUpItem.coords.Count; i++)
        {
            Vector2Int shapePosition = pickedUpItem.GetCurrentCoordinate(i);
            int x = Mathf.RoundToInt(gridCoordinates.x) + shapePosition.y;
            int y = Mathf.RoundToInt(gridCoordinates.y) + shapePosition.x;

            grid_occupation[x, y] = Occupation.Occupied;
        }
    }

    internal float FillRate()
    {
        float totalNumberOfCells = 0;
        float numberOfCellsOccupied = 0;

        foreach (Occupation occupation in grid_occupation)
        {
            if (occupation != Occupation.Blocked) totalNumberOfCells++;
            if (occupation == Occupation.Occupied) numberOfCellsOccupied++;
        }

        return numberOfCellsOccupied / totalNumberOfCells * 100f;
    }

    internal bool IsGridCompleted()
    {
        return FillRate() >= 80f;
    }

    private void ResetGridColors()
    {
        foreach (GameObject cell in grid)
        {
            if (cell != null)
                cell.GetComponent<SpriteRenderer>().color = Color.white;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.DrawSphere(transform.position + new Vector3(cell.transform.localScale.x * width, cell.transform.localScale.y * height,0), 0.5f);
    }
}
