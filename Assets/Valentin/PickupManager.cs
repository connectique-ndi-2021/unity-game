using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupManager : MonoBehaviour
{
    public Shape pickedUpItem = null;
    public Grid grid = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (pickedUpItem == null && Input.GetMouseButtonDown(0))
        {
            LayerMask mask = LayerMask.GetMask("Shape");
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 500.0f, mask);
            if (hit.collider != null)
            {
                Pickable pick = hit.collider.gameObject.GetComponent<Pickable>();

                if (!pick.transform.parent.GetComponent<ConveyorBelt>().dropped)
                {
                    if (pick != null && pick.isPickable)
                    {
                        pick.isPickable = false;
                        pickedUpItem = hit.collider.GetComponent<Shape>();
                        Destroy(hit.collider.GetComponentInParent<ConveyorBelt>());
                    }
                }

                
            }

        } else if (pickedUpItem != null)
        {
            Transform pickedUpItemTransform = pickedUpItem?.GetComponent<Transform>();
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            pickedUpItemTransform.position = new Vector3(mousePosition.x, mousePosition.y, pickedUpItemTransform.position.z);
        }
        
        if (pickedUpItem != null && Input.GetMouseButtonDown(1))
        {
            pickedUpItem?.GetComponent<Shape>()?.Rotate();
        }

        if (pickedUpItem != null && Input.GetMouseButtonDown(0))
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 500.0f);
            foreach (RaycastHit2D hit in hits)
            {
                if (hit.collider != null && hit.collider.GetComponent<Cell>() != null)
                {
                    if (grid.DoesShapeFits(hit.collider.GetComponent<Cell>().coordinates, pickedUpItem))
                    {
                        GameManager gameManager = FindObjectOfType<GameManager>();
                        grid.PlaceShape(hit.collider.GetComponent<Cell>().coordinates, pickedUpItem);
                        gameManager.AddScore(100);
                        gameManager.SetTrashBarValue(grid.FillRate() / 80f);
                        
                        pickedUpItem.transform.position = new Vector3(hit.collider.transform.position.x, hit.collider.transform.position.y, pickedUpItem.transform.position.z);
                        pickedUpItem = null;

                        if (grid.IsGridCompleted())
                        {
                            gameManager.NextLevel();
                        }
                    }
                    break;
                }

                if (hit.collider != null && hit.collider.GetComponent<Sea>() != null)
                {
                    pickedUpItem.GetComponent<Instanciator>().Create();
                    Destroy(pickedUpItem.transform.parent.gameObject);
                    FindObjectOfType<GameManager>().trashDropped(1);
                    pickedUpItem = null;
                }
            }
            
            //if (hits.collider != null)
            //{
            //    Pickable pick = hit.collider.gameObject.GetComponent<Pickable>();
            //    if (pick != null && pick.isPickable)
            //    {
            //        pick.isPickable = false;
            //        pickedUpItem = hit.collider.gameObject;
            //    }
            //}
        }
    }
}
