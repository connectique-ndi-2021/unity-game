using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour  
{
    public enum Orientation { Normal = 0, Right = 1, Inverse = 2, Left = 3}

    public Orientation orientation = Orientation.Normal;
    public List<Vector2Int> coords = new List<Vector2Int>();
    public GameObject cellPrefab;
    public GameObject[] _cells;

    // Start is called before the first frame update
    void Start()
    {
        float xScale = cellPrefab.transform.localScale.x;
        float yScale = cellPrefab.transform.localScale.y;

        _cells = new GameObject[coords.Count];

        for (int i = 0; i < coords.Count; i++)
        {
            Vector2Int coord = coords[i];
            GameObject go = Instantiate(cellPrefab, new Vector3(transform.position.x + coord.x * xScale, transform.position.y + coord.y * yScale, 0), Quaternion.identity, transform);
            //go.GetComponent<SpriteRenderer>().material.color = Color.tra;
            _cells[i] = go;
        }

        orientation = Orientation.Normal;
    }

    public void Rotate()
    {
        if ((int)orientation >= 3)
        {
            orientation = Orientation.Normal;
        } else
        {
            orientation = (Orientation)((int)orientation + 1);
        }

        for (int i = 0; i < coords.Count; i++)
        {
            Vector2Int current_coord = GetCurrentCoordinate(i);
            _cells[i].transform.localPosition = new Vector3(current_coord.x * _cells[i].transform.localScale.x, current_coord.y * _cells[i].transform.localScale.y, 0);
        }

        GetComponentInChildren<SpriteRenderer>().transform.rotation = Quaternion.Euler(0,0,90*(int)orientation);
    }

    public Vector2Int GetCurrentCoordinate(int i)
    {
        int current_x = coords[i].x;
        int current_y = coords[i].y;

        float new_x = current_x * Mathf.Cos(90 * (int)orientation * Mathf.Deg2Rad) - current_y * Mathf.Sin(90 * (int)orientation * Mathf.Deg2Rad);
        float new_y = current_y * Mathf.Cos(90 * (int)orientation * Mathf.Deg2Rad) + current_x * Mathf.Sin(90 * (int)orientation * Mathf.Deg2Rad);

        return new Vector2Int(Mathf.RoundToInt(new_x), Mathf.RoundToInt(new_y));
    }
}
