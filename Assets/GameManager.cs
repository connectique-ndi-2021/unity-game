using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{

    public ObjectSpawner ObjectSpawner;
    public HealthBarWithIndicator HealthBarWithIndicator;
    public HealthBarWithIndicator TrashBarWithIndicator;

    public UnityEvent nextLevel;

    public int healthMax;
    private int health;

    void Start()
    {
        levelText.text = $"Level {FindObjectOfType<GameData>().level}";
        AddScore(0);

        Time.timeScale = 1.0f;

        health = healthMax;

        HealthBarWithIndicator.SetValue(1);
        TrashBarWithIndicator.SetValue(0);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void trashDropped(int damage)
    {
        health -= damage;
        HealthBarWithIndicator.SetValue((float)health / healthMax);
        if(health <= 0)
        {
            OpenGameOverMenu();
        }
        

    }

    public void SetTrashBarValue(float value)
    {
        TrashBarWithIndicator.SetValue(value);
    }


    // Start is called before the first frame update
    

    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI levelText;

    public void AddScore(int score)
    {

        FindObjectOfType<GameData>().score += score * FindObjectOfType<GameData>().level;


        scoreText.text = FindObjectOfType<GameData>().score.ToString("D8");

    }

    public void NextLevel()
    {
        FindObjectOfType<GameData>().level += 1;
        FindObjectOfType<Grid>().obstacleRate = (float)(FindObjectOfType<GameData>().level) * 0.05f;

        nextLevel.Invoke();
    }



    public Canvas GameOverCanvas;
    public TextMeshProUGUI scoreTextGameOver;

    public static readonly string HighScorePref = "highscore";

    public void OpenGameOverMenu()
    {
        Time.timeScale = 0f;

        int score = FindObjectOfType<GameData>().score;


        if (PlayerPrefs.HasKey(HighScorePref))
        {
            int highscore = PlayerPrefs.GetInt(HighScorePref);

            if (score > highscore)
            {
                PlayerPrefs.SetInt(HighScorePref, score);

                scoreTextGameOver.text = $"NEW HIGHSCORE\n{score}";
            }
            else
            {
                scoreTextGameOver.text = $"HIGHSCORE\n{highscore}\nYOUR SCORE\n{score}";
            }
        }
        else
        {
            PlayerPrefs.SetInt(HighScorePref, score);

            scoreTextGameOver.text = $"NEW HIGHSCORE\n{score}";
        }

        GameOverCanvas.gameObject.SetActive(true);


    }




}
