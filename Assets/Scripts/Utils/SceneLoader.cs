﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    bool isLoading;

    public Sound SoundStart;


    public void LoadNextScene()
    {
        if (isLoading) return;

        if (SoundStart != null)
            AudioManager.AUDIO_MANAGER.PlayAudioGlobal(SoundStart, SoundStart.clip.name);

        isLoading = true;

            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        
        
    }

    public void LoadScene(string name)
    {
        if (isLoading) return;
        isLoading = true;
        SceneManager.LoadSceneAsync(SceneManager.GetSceneByName(name).buildIndex);

    }

    public void LoadScene(int index)
    {
        if (isLoading) return;
        isLoading = true;
        SceneManager.LoadSceneAsync(index);

    }

    public void ReloadScene()
    {
        if (isLoading) return;
        isLoading = true;
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex);
    }


}
