﻿using UnityEngine;

public static class Vectors
{

    public static Vector2 TransformToVector2(Transform transform) {
        return new Vector2(transform.position.x, transform.position.y);
    }

}
