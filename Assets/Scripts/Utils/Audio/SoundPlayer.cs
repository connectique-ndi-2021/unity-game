﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    public Sound sound;

    // Start is called before the first frame update
    void Start()
    {
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sound, sound.clip.name);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
