﻿using UnityEngine.Audio;
using UnityEngine;

[System.Serializable]
public class Sound
{

    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 0.5f;
    [Range(0.1f, 3f)]
    public float pitch = 1;


    [Range(0f, 1f)]
    public float spacialBlend = 0;

    public bool loop = false;

    [HideInInspector]
    public AudioSource source;



    public AudioMixerGroup mixer;


}
