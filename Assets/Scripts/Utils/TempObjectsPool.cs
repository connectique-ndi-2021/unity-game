﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempObjectsPool : MonoBehaviour
{

    public static Transform POOL;


    private void Awake()
    {
        if (POOL != null)
        {
            Debug.LogWarning("Only use one tempPool !!! static object " + this.name);
            Destroy(this);
        }
        POOL = this.transform;
    }
}
