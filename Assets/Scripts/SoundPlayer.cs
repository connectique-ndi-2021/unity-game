﻿using UnityEngine;
using UnityEngine.Events;

public class TestScript : MonoBehaviour
{
    public UnityEvent function;
    public KeyCode keyCode = KeyCode.A;

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(keyCode)) {
            function.Invoke();
        }
    }
}
