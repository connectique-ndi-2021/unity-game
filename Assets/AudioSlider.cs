﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class AudioSlider : MonoBehaviour
{

    //public Mixer mixer;

    public Slider slider;
    public AudioMixer mixer;

    public string mixerName = "MainVolume";

    public void Start()
    {
        if(mixer.GetFloat(mixerName, out float value) )
            slider.value = value;
    }

    public void OnValueChanged()
    {
        mixer.SetFloat(mixerName, slider.value);
    }

}
